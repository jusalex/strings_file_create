package com.akenori;

public class Translation {

    private String key;
    private String ru;
    private String en;
    private String ua;
    private String pl;
    private String de;
    private String es;
    private String ja;
    private String zh;

    public Translation(String key, String ru, String en, String ua, String pl, String de, String es, String ja, String zh) {
        this.key = key;
        this.ru = ru;
        this.en = en;
        this.ua = ua;
        this.pl = pl;
        this.de = de;
        this.es = es;
        this.ja = ja;
        this.zh = zh;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getRu() {
        return ru;
    }

    public void setRu(String ru) {
        this.ru = ru;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getPl() {
        return pl;
    }

    public void setPl(String pl) {
        this.pl = pl;
    }

    public String getDe() {
        return de;
    }

    public void setDe(String de) {
        this.de = de;
    }

    public String getEs() {
        return es;
    }

    public void setEs(String es) {
        this.es = es;
    }

    public String getJa() {
        return ja;
    }

    public void setJa(String ja) {
        this.ja = ja;
    }

    public String getZh() {
        return zh;
    }

    public void setZh(String zh) {
        this.zh = zh;
    }

}
