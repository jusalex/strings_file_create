package com.akenori;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.akenori.Make.LOCALES.*;

public class Make {

    private static final int KEY_NUM_0 = 0;
    private static final int RU_NUM_1 = 1;
    private static final int EN_NUM_2 = 2;
    private static final int UA_NUM_3 = 3;
    private static final int PL_NUM_4 = 4;
    private static final int DE_NUM_5 = 5;
    private static final int ES_NUM_6 = 6;
    private static final int JA_NUM_7 = 7;
    private static final int ZH_NUM_8 = 8;
    private static final String HEADER = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    private static final String RESOURCES_START = "<resources>";
    private static final String RESOURCES_END = "</resources>";
    private static final String DIR = "/Users/ayurtaev/Downloads/Translation_2/";

    enum LOCALES {EN, RU, UA, PL, DE, ES, JA, ZH}

    public static void main(String[] args) throws Exception {
        System.setProperty("org.apache.poi.util.POILogger", "org.apache.poi.util.CommonsLogger");

        File sourceFile = new File(DIR + "android_translations.xlsx");

        List<Translation> translations;
        try (FileInputStream fis = new FileInputStream(sourceFile)) {
            XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
            XSSFSheet mySheet = myWorkBook.getSheetAt(0);
            Iterator<Row> rowIterator = mySheet.iterator();

            translations = new ArrayList<>();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                try {
                    translations.add(new Translation(
                            row.getCell(KEY_NUM_0, Row.RETURN_BLANK_AS_NULL).getStringCellValue(),
                            row.getCell(RU_NUM_1, Row.RETURN_BLANK_AS_NULL).getStringCellValue(),
                            row.getCell(EN_NUM_2, Row.RETURN_BLANK_AS_NULL).getStringCellValue(),
                            row.getCell(UA_NUM_3, Row.RETURN_BLANK_AS_NULL).getStringCellValue(),
                            row.getCell(PL_NUM_4, Row.RETURN_BLANK_AS_NULL).getStringCellValue(),
                            row.getCell(DE_NUM_5, Row.RETURN_BLANK_AS_NULL).getStringCellValue(),
                            row.getCell(ES_NUM_6, Row.RETURN_BLANK_AS_NULL).getStringCellValue(),
                            row.getCell(JA_NUM_7, Row.RETURN_BLANK_AS_NULL).getStringCellValue(),
                            row.getCell(ZH_NUM_8, Row.RETURN_BLANK_AS_NULL).getStringCellValue()
                    ));
                } catch (Exception e) {
                    break;
                }
            }
        }
        translations.remove(0);

        writeFile(translations, RU);
        writeFile(translations, EN);
        writeFile(translations, UA);
        writeFile(translations, PL);
        writeFile(translations, DE);
        writeFile(translations, ES);
        writeFile(translations, JA);
        writeFile(translations, ZH);
    }

    private static void writeFile(List<Translation> translations, LOCALES locale) throws IOException {
        File outFile = new File(DIR + locale.name() + ".xml");
        if (outFile.exists()) {
            outFile.delete();
        }
        try (FileWriter fileWriter = new FileWriter(outFile)) {
            fileWriter.write(HEADER);
            fileWriter.write(System.lineSeparator());
            fileWriter.write(RESOURCES_START);
            fileWriter.write(System.lineSeparator());
            translations.forEach(translationItem -> {
                try {
                    String translation = "";
                    switch (locale) {
                        case EN:
                            translation = translationItem.getEn();
                            break;
                        case RU:
                            translation = translationItem.getRu();
                            break;
                        case UA:
                            translation = translationItem.getUa();
                            break;
                        case PL:
                            translation = translationItem.getPl();
                            break;
                        case DE:
                            translation = translationItem.getDe();
                            break;
                        case ES:
                            translation = translationItem.getEs();
                            break;
                        case JA:
                            translation = translationItem.getJa();
                            break;
                        case ZH:
                            translation = translationItem.getZh();
                    }
                    fileWriter.write(String.format("    <string name=\"%s\">%s</string>%n", translationItem.getKey(), translation));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            fileWriter.write(RESOURCES_END);
        }
    }
}
